SRC_COMMON:=$(wildcard QuasiAffineTransform/common/*.cpp)
HDR_COMMON:=$(wildcard QuasiAffineTransform/common/*.hpp)
OBJ_COMMON:=$(SRC_COMMON:.cpp=.o)
SRC_2D:=$(wildcard QuasiAffineTransform/2D/*.cpp)
HDR_2D:=$(wildcard QuasiAffineTransform/2D/*.hpp)
OBJ_2D:=$(SRC_2D:.cpp=.o)
SRC_3D:=$(wildcard QuasiAffineTransform/3D/*.cpp)
HDR_3D:=$(wildcard QuasiAffineTransform/3D/*.hpp)
OBJ_3D:=$(SRC_3D:.cpp=.o)
SRC_LIBVOL:=$(wildcard libvol/*.cpp)
HDR_LIBVOL:=$(wildcard libvol/*.hpp)
OBJ_LIBVOL:=$(SRC_LIBVOL:.cpp=.o)
CXXFLAGS:=`pkg-config --cflags ImageMagick++ tclap` -Ilibvol
LDLIBS:=`pkg-config --libs ImageMagick++ tclap` -lstdc++ -lm

all: qat-2D qat-3D

qat-2D: QuasiAffineTransform/2D/qat-2D
	cp $< $@

qat-3D: QuasiAffineTransform/3D/qat-3D
	cp $< $@

QuasiAffineTransform/2D/qat-2D: $(OBJ_2D) $(OBJ_COMMON)

QuasiAffineTransform/3D/qat-3D: $(OBJ_3D) $(OBJ_COMMON) $(OBJ_LIBVOL)

QuasiAffineTransform/common/%.o: $(SRC_COMMON) $(HDR_COMMON)

QuasiAffineTransform/2D/%.o: $(SRC_2D) $(HDR_2D) $(HDR_COMMON)

QuasiAffineTransform/3D/%.o: $(SRC_3D) $(HDR_3D) $(HDR_COMMON)

libvol/%.o: $(SRC_LIBVOL) $(HDR_LIBVOL)

clean:
	$(RM) $(OBJ_COMMON) $(OBJ_2D) $(OBJ_3D) $(OBJ_LIBVOL) QuasiAffineTransform/2D/qat-2D QuasiAffineTransform/2D/qat-3D qat-2D qat-3D

