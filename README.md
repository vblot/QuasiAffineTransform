LICENSE
=======

- GPLv2, see LICENSE file


How to build it
===============

To compile this code, you would need:

- Imagemagick++ libraries to import 2d image files to transform
- tclap library to parse command-line options
- libvol to import 3d vol files (https://liris.cnrs.fr/david.coeurjolly/code/simplevol.html)

The Makefile is included, a simple "make" should build the two binaries. Refer to README files in subdirectories "QuasiAffineTransform" and "libvol" for details.
The built files are qat-2D and qat-3D


Viewing .vol files
==================

Viewing the ".vol" files requires the "qvox" tool.
Original version is here: http://qvox.sourceforge.net
Patched version for latest Qt and gcc is here: https://gitlab.inria.fr/vblot/qvox
